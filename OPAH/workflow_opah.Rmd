---
title: "Workflow des OPAH"
author: "Juliette ENGELAERE-LEFEBVRE"
date: "`r format(Sys.Date(), '%d/%m/%Y')`" 
output: html_document
params:
  annee: "2022" # annee de la carte com et epci utilisée
  sgbd: TRUE # mettre à TRUE pour almimenter le SGBD quand traitements OKs
---

```{r setup, include=FALSE, message = FALSE}
# options de compilation rmd
knitr::opts_chunk$set(echo = FALSE)
options(knitr.kable.NA = '')

# chargements des packages
library(tidyverse)
library(lubridate)
# devtools::install_github("maeltheuliere/COGiter")
library(COGiter)
library(readODS)
library(RPostgres)
library(datalibaba)
library(rpostgis)
library(DBI)
library(sf)
# devtools::install_github("pachevalier/tricky")
library(tricky)

```

# Lecture des données Opah

## Découverte du contenu du répertoire OPAH et selection des fichiers à importer
Les fichiers transmis par DPH sont à enregistrer dans le dossier `TableauxSuiviZonages/OPAH`.
> Le script de traitement va détecter les fichiers à importer à partir d'un motif de texte à respecter lors de chaque envoi, et en prenant le dernier par ordre alphabétique.  
L'extraction ANAH est à fournir au format csv et doivent contenir "Extraction 2".  
Le tableur relatif aux nouvelles OPAH doit contenir "OPAH_nouvelles" est être transmis au format ods.


```{r fichier}

repo_ext_data <- "T:/datalab/SIAL/ZONAGES_HABITAT/extdata/TableauxSuiviZonages/OPAH/"
fich <- list.files(repo_ext_data, full.names = FALSE, recursive = TRUE)

fic <- list()

fic$si_anah_2 <- grep("Extraction.2.+\\.csv$", fich, value = TRUE) %>% 
  sort(decreasing = TRUE) %>% 
  head(1)

fic$opah_com_new <- grep("OPAH_nouvelles.+\\.ods$", fich, value = TRUE) %>% 
  sort(decreasing = TRUE) %>% 
  head(1)

adr <- paste0(repo_ext_data, fic)

```

La mise à jour des OPAH va se baser sur les fichiers `r paste0(fic, collapse = " et ")`.

## Export ANAH 2 - liste des OPAH avec données attributaires

On utilise la 2e extraction du SI de l'ANAH pour dresser la liste des OPAH en cours. En voici les 5 premières lignes :

```{r lecture des donnees, message=FALSE}
# Création de la table des OPAH en cours
opah_0 <- read_csv2(adr[1], col_names = TRUE, locale = locale(decimal_mark = ",", encoding = "Latin1")) %>% 
  set_standard_names()

opah_1 <- opah_0 %>%
  # on corrige les caractères cabalistiques dans le nom des champs
  rename_with(.fn =  ~ gsub("\ufffd", "e", .x)) %>% 
  # on nettoie le contenu des champs et on type les variables
  mutate(across(everything(), ~ gsub("<br/>", "", .x)),
         var_1_departement = gsub("^0", "", var_1_departement),
         var_1_territoires_de_gestion = gsub("\ufffd", "E", var_1_territoires_de_gestion),
         var_1_operateur = gsub("\ufffd", "E", var_1_operateur),
         across(starts_with("var_2"), ~ as.Date(.x, format = "%d/%m/%Y")),
         across(c(var_1_departement, var_1_territoires_de_gestion, var_1_types_de_programme, var_1_operateur), as.factor),
         across(starts_with("var_3"), as.numeric)) %>%
  # on supprime les informations qui relèvent du bilan de gestion
  select(-zz, -starts_with("var_4"), -var_5_logements_subventionnes) %>%
  # on nettoie et raccourcit le nom des champs
  rename_with(.fn = ~ gsub("var_._|_$", "", .x) %>%
                gsub("_de_|_du_|_des_", "_", .) %>%
                gsub("objectifs", "objectif", .)) %>% 
  # on redresse le type d'OPAH de Mayenne Communaute saisi comme OPAH-RU alors que plusieurs volets
  mutate(types_programme = if_else(identifiant == "053OPA014", "OPAH", as.character(types_programme)) %>% as.factor) %>% 
  rename(dep = departement, 
         prog_com_infracom = commune_prog_communaux_ou_infra_communaux,
         territoire_gestion = territoires_gestion,
         type_programme = types_programme,
         objectif_financier = objectif_montants_financiers,
         objectif_nb_lgt = objectif_lgts_subventionnes,
         nom = libelle_programme,
         id_opah = identifiant) %>%
  relocate(prog_com_infracom, .after = last_col()) %>%
  # on ne garde que les OPAH
  filter(grepl("OPAH", type_programme))

# Visualisation du résultat
knitr::kable(head(opah_1, 5)) %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))
```
Il manque à cette liste, l'OPAH inter-régionale de Redon, on l'ajoute à partir des anciennes données tant que l'OPAH n'a pas expirée.
Chargement de la couche OPAH préexistante à partir du SGBD : `opah_r52`.
```{r Ajout Redon}

opah_old <- importer_data(table = "n_opah_zope_r52", schema = "zonages_habitat", db = "production", user = "does" ) %>% 
  mutate(dep = as.character(dep))
# poster_data(data = opah_old, table = "n_opah_zope_r52", schema = "zonages_habitat", db = "production", user = "does", pk = NULL, overwrite = TRUE)

opah_2 <-  opah_old %>% 
  # on ne garde que les attributs présents dans l'extraction 2 Anah
  st_drop_geometry() %>% 
  select(any_of(names(opah_1))) %>% 
  # et les opérations relevant des départements voisins
  filter(!(dep %in% c("44", "49", "53", "72", "85"))) %>% 
  # assemblage avec la nouvelle liste des OPAH.
  bind_rows(opah_1, .)

```


## Composition communale des OPAH

### OPAH dejà connues

Chargement des couches OPAH com et infracom préexistantes à partir du SGBD : `opah_com_r52` et `opah_infracom_r52`.  
```{r lecture des donnees old, warning=FALSE}

# Lecture des précédentes couches
opah_com_old <- importer_data(table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does" )
# poster_data(data = opah_com_old, table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does", pk = NULL, overwrite = TRUE)

opah_infra <- importer_data(table = "opah_infracom_r52", schema = "zonages_habitat", db = "production", user = "does") %>% 
  # on ajoute aux OPAH infra les codes et noms de communes 
  st_join(communes_geo, left = TRUE, largest = FALSE) %>% 
  select(-AREA) %>% 
  left_join(communes %>% select(DEPCOM, NOM_DEPCOM), by = "DEPCOM") %>% 
  group_by(id_opah) %>% 
  summarise(id_com_infra = list(as.character(DEPCOM)), com_infra = list(as.character(NOM_DEPCOM)), geometry = st_union(geometry)) %>% 
  rowwise %>% 
  mutate(id_com_infra = paste(id_com_infra, collapse = ", "),
         com_infra = paste(com_infra, collapse = ", ")) %>% 
  ungroup()
```

### Nouvelles OPAH

Le contour géo des nouvelles OPAH ne peut pas être déterminé à partir de l'extraction 1 qui, du point de vue de l'administration des zonages n'apporte pas d'informations exploitables.
Voyons, par différence avec les OPAH figurant dans le SGBD, quelles sont les nouvelles OPAH figurent dans l'extraction ANAH type 2 :

```{r opah new}

opah_new <- filter(opah_2, !(id_opah %in% opah_old$id_opah))

# Visualisation du résultat

opah_new %>% select(id_opah, contains("prog")) %>% 
  knitr::kable() %>%
  kableExtra::kable_styling(bootstrap_options = c("striped", "hover", "condensed"))

```

On constitue une table de passage com/opah à partir de l'[info-centre public ANAH : http://infocentre.anah.gouv.fr/latitudes/RunFilter.asp?cdEntite=CONVENTION&cdRequete=ProgFiche&idPresentation=9903](http://infocentre.anah.gouv.fr/latitudes/RunFilter.asp?cdEntite=CONVENTION&cdRequete=ProgFiche&idPresentation=9903).  

> Cette table comprend 5 colonnes :   

>- identifiant : l'identifiant de l'OPAH,   
>- depcom : le code insee de la commune constitutive de l'OPAH,  
>- epci_complet : un booléen (TRUE/FALSE) indiquant si le programme concerne tout l'EPCI,  
>- commentaire : indiquer le cas échéant "OPAH simple comprenant un volet RU",  
>- nom_court : nom d'étiquette que l'on souhaite voir figurer dans la carte QGIS.

> Lors de la prochaine mise à jour, ces informations seront à fournir par DPH, en complément des infos permettant de construire les périmètres infra-communaux.

En voici les 10 premières lignes.
```{r opah new 2}
opah_com_new <- read_ods(adr[2], col_types = cols(identifiant = col_character(), depcom = col_character(), 
                                                  epci_complet = col_logical(), commentaire	 = col_character(), 
                                                  nom_court = col_character())) %>% 
  rename(id_opah = identifiant)
head(opah_com_new, 10)
```

On vérifie que toutes les nouvelles OPAH de l'export ANAH figurent bien dans la table communale ods :  

```{r opah new verif}
verif1 <- opah_new %>% select(id_opah, nom) %>% mutate(ok = id_opah %in% opah_com_new$id_opah)
stop <- !all(verif1$ok)
verif1
```

```{r knitexit1, eval=stop}
if(stop) {
  warning(paste0("Il manque dans ", fic[3], " la composition communale de ", paste0(filter(verif1, !ok) %>%  pull(id_opah), 
                                                                                    collapse = ", ")))
  knitr::knit_exit(fully = TRUE)
  }
```


On vérifie si des contours infra communaux sont à produire, soit parce que le champ *prog_com_infracom* issu de l'export ANAH type 2 est rempli, soit pcq la table fournie par DPH sur les nouvelles OPAH contient un commentaire : "OPAH simple comprenant un volet RU". 
Liste des opah infra-com nouvelles : 

```{r opah new verif infracom}
opah_infra_new_tbl <- opah_new %>% 
  filter(!is.na(prog_com_infracom), type_programme == "OPAH-RU") %>% 
  select(id_opah, nom, prog_com_infracom) %>% 
  bind_rows(filter(opah_com_new, commentaire == "OPAH simple comprenant un volet RU" | 
                     commentaire == "OPAH à volet renouvellement urbain") %>% 
              select(id_opah, nom = nom_court)) %>% 
  group_by(id_opah) %>% 
  summarise(nom = first(nom), .groups = "drop")

opah_infra_new_tbl

opah_infra_new <- nrow(opah_infra_new_tbl) > 0
k_exit_2 <- FALSE # initialisation du parametre d'arret

```
On vérifie maintenant que tous les contours infracom dont on a besoin sont dans la couche SGBD adhoc.
```{r opah new verif infracom 2, eval = opah_infra_new }

verif_infra_com <- opah_2 %>% 
  # sélection des OPAH infra com : on exclut l'OPAH particulière de l'Ile d'Yeu qui concerne une seule commune mais en entier.
  filter(!is.na(prog_com_infracom) | type_programme != "OPAH", id_opah !="085OPA023") %>% # 
  bind_rows(filter(opah_com_new, commentaire == "OPAH simple comprenant un volet RU")) %>% 
  # select(id_opah) %>% 
  mutate(ok = id_opah %in% opah_infra$id_opah) %>% 
  filter(!ok)

k_exit_2 <- (nrow(verif_infra_com) > 0) 

```

```{r knitexit2, eval=k_exit_2, echo = FALSE}
if(k_exit_2) {
  warning(paste0("Il manque dans zonages_habitat.opah_infracom_r52 les contours infra-communaux des OPAH : ", 
                 paste0(verif_infra_com$id_opah, collapse = ", ")))
  
  knitr::knit_exit(fully = TRUE)
}
```

# Mise à jour du cog

## Mise à jour du COG des communes

Après assemblage des tables communales ancienne et nouvelle, on met à jour le code commune des OPAH toujours actives.
```{r maj COG com}
opah_com_0 <- opah_com_new %>% 
  # assemblage
  bind_rows(opah_com_old) %>%
  # on ne met à jour que les OPAH actives
  filter(id_opah %in% opah_2$id_opah) %>% 
  # mise à jour du code commune
  passer_au_cog_a_jour(code_commune = "depcom", aggrege = FALSE, garder_info_supra = FALSE) %>% 
  # en cas de fusion de communes, des doublons apparaissent
  distinct() %>% 
  # la mise à jour du COG impacte le champ code commune, on repasse les noms de colonnes en minuscules et le champ depcom au format texte
  rename_with(tolower) %>% 
  mutate(depcom = as.character(depcom)) %>% 
  select(names(opah_com_old))
  
```
## Mise à jour du COG des EPCI

Pour tenir compte des éventuelles modifications du contour des EPCI, on évalue si la composition communale des OPAH couvrant tout un EPCI correspond toujours aux contours de l'EPCI, ou au contraire, s'il manque une commune ayant nouvellement rejoint l'EPCI ou si une commune a quitté le périmètre de l'OPAH en rejoignant un autre EPCI.

```{r maj COG EPCI 1}
# liste des communes de chaque EPCI de la région
epci_r52 <- filter(communes, grepl("52", REGIONS_DE_L_EPCI)) %>% 
  group_by(EPCI) %>% 
  summarise(list_com = list(as.character(DEPCOM)))

# situation de chaque OPAH intercommunale vis à vis des nouveaux contours EPCI
opah_epci <- opah_com_0 %>% 
  # on garde les OPAH identifiées comme intercommunales
  filter(epci_complet) %>% 
  # on ajoute les nouveaux codes EPCI d'appartenance de chaque commune de l'OPAH
  left_join(select(communes, DEPCOM, EPCI), by = c("depcom" = "DEPCOM")) %>% 
  # on ajoute la colonne de comparaison comprenant la liste des communes à jour des EPCI
  left_join(epci_r52, by = "EPCI") %>% 
  # pour chaque OPAH et chaque EPCI, on compare les listes de communes initiales et à jour
  group_by(id_opah, EPCI) %>% 
  summarise(epci_complet = all(unlist(list_com) %in% c(depcom)), .groups = "drop")

# Y a -t-il des OPAH intercommunales pour lesquelles la composition communale ne correspond plus aux contour de son EPCI ?
modif <- !all(opah_epci$epci_complet)

# Y a -t-il des OPAH intercommunales à cheval sur 2 EPCI ?
opah_cheval_epci <- opah_epci %>% 
  group_by(id_opah) %>% 
  summarise(nb_epci = n_distinct(EPCI), EPCI = list(EPCI)) %>% 
  filter(nb_epci>1) %>% 
  left_join(select(opah_2, id_opah, type_programme, nom))

cheval <- nrow(opah_cheval_epci) > 0
```

Y a-t-il des modifications à apporter aux contours EPCI des OPAH ? `r modif`
```{r maj COG EPCI 2, eval=modif}
filter(opah_epci, !epci_complet)
```
Nota 2022 : il manque les communes de l'Orne de la nouvelle opération 'OPAH COMMUNAUTE DE COMMUNES MAINE SAOSNOIS'.


Y a-t-il des OPAH intercommunales désormais à cheval sur 2 EPCI ? `r cheval`

```{r maj COG EPCI 3, eval=cheval, message=FALSE, warning=FALSE}
opah_cheval_epci
```



On actualise la composition communale de chaque OPAH intercommunale, en affectant à chacune la composition communale à jour de l'EPCI (principal) constitutif de l'OPAH. 
```{r maj COG EPCI 4}
# on part de la table communale des OPAH mise à jour du COG
opah_com_epci <- opah_com_0 %>% 
  # on garde les OPAH aux contours EPCI (grâce à l'info epci_complet de l'ancienne table com ou du tableur des nouvelles opah)
  filter(epci_complet) %>% 
  # on adjoint les codes EPCI aux communes
  left_join(select(communes, DEPCOM, EPCI), by = c("depcom" = "DEPCOM")) %>% 
  # on trie la table par identifiant OPAH, identifiant EPCI et code commune
  arrange(id_opah, EPCI, depcom) %>% 
  # on crée un groupe par OPAH
  group_by(id_opah, EPCI) %>% 
  # dont on ne garde qu'une ligne par OPAH et EPCI comportant le nb de communes correspondantes
  summarise(nb_com_epci = n_distinct(depcom), .groups = "drop_last") %>% 
  # on garde l'EPCI principal, ie avec le plus grand nombre de communes
  filter(nb_com_epci == max(nb_com_epci)) %>% 
  # on adjoint un champ à jour listant toutes les communes de l'EPCI
  left_join(epci_r52, by = "EPCI") %>%
  # on déplie la liste des communes
  unnest(list_com) %>% 
  select(id_opah, depcom = list_com) %>% 
  mutate(epci_complet = TRUE)

# assemblage des tables communales d'OPAH EPCI et communes
# on part de la table communale des OPAH mise à jour du COG
opah_com_1 <- opah_com_0 %>% 
  # on garde seulement les OPAH aux contours communaux
  filter(!epci_complet) %>% 
  # qu'on assemble aux OPAH aux contours d'EPCI mises à jour ci dessus
  bind_rows(opah_com_epci)
```

## Mise à jour de la table communale du SGBD

On actualise la table `opah_com_r52` du SGBD, suite à la mise à jour du COG.
```{r, eval = params$sgbd}

poster_data(data = opah_com_1, table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does", 
            overwrite = TRUE, pk = c("id_opah", "depcom"))

commenter_champs(comment = "Identifiant de l'OPAH", var = "id_opah", table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does")
commenter_champs(comment = "Code INSEE des communes constitutives de l OPAH", var = "depcom", table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does")
commenter_champs(comment = "OPAH portant sur l'ensemble du contour EPCI", var = "epci_complet", table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does")

comntr <- paste0("Table intermédiaire nécessaire à la production et la maintenance de la couche n_opah_zope_r52. ", 
                 "Elle fournit la composition communale des OPAH et a été mise a jour le ",
                 format(Sys.Date(), '%d %m %Y'), ". Elle est nécessaire a la maintenance du COG.")
commenter_table(comntr, table = "opah_com_r52", schema = "zonages_habitat", db = "production", user = "does")

```


# Mise à jour de la table des opah en cours

## Attributs nom_court et commentaire

Les attributs `nom_court` et `commentaires` sont repartis entre la table des anciennes OPAH SGBD et le tableur calc des nouvelles OPAH. On les assemble.

```{r attributs epars}
opah_attr <- opah_com_new %>% 
  select(id_opah, nom_court, commentaire) %>% 
  bind_rows(select(opah_old, id_opah, nom_court, commentaire) %>% st_drop_geometry()) %>% 
  distinct()
```

## Attribut EPCI

Les OPAH sont des programmes décidés à l'échelle des EPCI, DPH a demandé à ce que la table géo des OPAH contiennent cette information.  
On vérifie d'abord que toutes les OPAH ne relèvent que d'un et d'un seul EPCI.
```{r opah att EPCI test, warning=FALSE}

test_appartenance_epci <- opah_com_1 %>% left_join(communes %>% select(DEPCOM, EPCI, NOM_EPCI), by=c("depcom" = "DEPCOM")) %>% 
  mutate(across(contains("EPCI"), as.character)) %>% 
  group_by(id_opah) %>% 
  summarise(nb_EPCI = n_distinct(EPCI), NOM_EPCI = list(NOM_EPCI), EPCI = list(EPCI), .groups = "drop") %>% 
  filter(nb_EPCI >1)

stop_opah_cheval_epci <- nrow(test_appartenance_epci) > 1

```

```{r knitexit3, eval=stop_opah_cheval_epci, echo = FALSE}
if(stop_opah_cheval_epci) {
  warning(paste0("On a au moins une OPAH à cheval sur deux EPCI : ", 
                 paste0(test_appartenance_epci$id_opah, collapse = ", "),
                 ". Revoir les scripts de mise à jour des contours ou le script (paragraphe attribut EPCI ci dessous) pour choisir le bon avec DPH." 
                 ))
  knitr::knit_exit(fully = TRUE) 
}
```

On crée ensuite la table identifiant OPAH-EPCI d'appartenance en vue de l'ajout de cet attribut dans la table géo des opah.
```{r opah att EPCI, warning=FALSE}

opah_epci <- opah_com_1 %>% 
  left_join(communes %>% select(DEPCOM, EPCI, NOM_EPCI), by=c("depcom" = "DEPCOM")) %>% 
  mutate(across(contains("EPCI"), as.character)) %>% 
  select(id_opah, EPCI, NOM_EPCI) %>% 
  distinct()

# menage
rm(stop_opah_cheval_epci, test_appartenance_epci)

```



## Assemblage geo des communes et ajout attributs
```{r}
opah_3 <- opah_com_1 %>% 
  right_join(select(communes_geo, -AREA), ., by = c("DEPCOM" = "depcom")) %>% 
  select(-epci_complet) %>% 
  group_by(across(-c(DEPCOM, geometry))) %>%
  summarise(nb_communes = n(), do_union = TRUE, .groups = "drop") %>%
  left_join(opah_2, by = "id_opah") %>% 
  left_join(opah_attr, by = "id_opah") %>% 
  left_join(opah_epci, by = "id_opah") %>% 
  mutate(temporalite = case_when(
           today() >= date_debut & today() <= date_expiration ~ "encours",
           today() < date_debut ~ "projet",
           today() > date_expiration ~ "close",
           TRUE ~ NA_character_)) %>% 
  select(any_of(names(opah_old)), EPCI, NOM_EPCI)

```

## Substitution des contours communaux par les contours infracom du SGBD pour les OPAH infracommunales

```{r}
opah_4 <- opah_3 %>% 
  # on ajoute des lignes pour tenir compte des OPAH doubles (OPAH et OPAH-RU)
  bind_rows(filter(opah_3, commentaire == "OPAH simple comprenant un volet RU") %>% 
            mutate(type_programme = "OPAH-RU", nb_communes = NA_integer_)) %>% 
  # on transformes les géométries en texte pour pouvoir ensuite choisir celles qu'on retient
  st_transform(2154) %>%
  mutate(geometry1 = st_as_text(geometry)) %>% 
  st_drop_geometry() %>% 
  # on joint la table des géométries infra-communales
  left_join(opah_infra %>% 
              mutate(geom_infra = st_as_text(geometry)) %>% 
              st_drop_geometry(),
            by = c("id_opah" = "id_opah")) %>% 
  # on détermine quelle géométrie retenir
  mutate(the_geom = case_when(is.na(geom_infra) ~ geometry1, 
                              commentaire == "OPAH simple comprenant un volet RU" & type_programme == "OPAH" ~ geometry1, 
                              TRUE ~ geom_infra) %>% st_as_sfc(2154),
         # le nb de communes du volet RU des opah doubles à partir du nb de caractères du champ id_com_infra
         nb_communes = ifelse(is.na(nb_communes), (nchar(id_com_infra) + 2)/7, nb_communes),
         # et on nettoie les champs id_com_infra et com_infra du volet OPAH des opah doubles joints à tort
         id_com_infra = if_else(type_programme == "OPAH", NA_character_, id_com_infra),
         com_infra = if_else(type_programme == "OPAH", NA_character_, com_infra)) %>% 
  select(-geometry1, -geom_infra) %>% 
  distinct %>% 
  st_as_sf() %>% 
  unique() %>% 
  arrange(id_opah, desc(type_programme)) %>% 
  relocate(the_geom, .after = everything()) 

mapview::mapview(opah_4, zcol = c("id_opah"), burst = TRUE, legend = FALSE)
```


## Mise à jour de la table OPAH zopé du SGBD

On sauvegarde la table des OPAH dans deux tables SGBD, l'une correspond au nom court `n_opah_zope_r52`, l'autre comporte dans son nom l'année en cours. Ainsi, l'archivage des couches annuelles se fera automatiquement.

```{r, eval=params$sgbd}
poster_data(data = opah_4, schema = "zonages_habitat", table = "n_opah_zope_r52", db = "production", overwrite = TRUE, user = "does",
            pk = c("id_opah", "type_programme"))
commenter_table("Table des OPAH en cours, périmètres opérationnels.", table = "n_opah_zope_r52", 
                schema = "zonages_habitat", db = "production", user = "does")


poster_data(data = opah_4, schema = "zonages_habitat", table = paste0("n_opah_zope_", year(today()) , "_r52"), 
            db = "production", overwrite = TRUE, user = "does")

commenter_table(comment = paste0("Table des OPAH ",  year(today())), table = paste0("n_opah_zope_", year(today()) , "_r52"), 
                schema = "zonages_habitat", db = "production", user = "does")


```
## Création d'une table de synthèse communale sur les OPAH

DPH souhaite disposer d'une vision de quelles communes est impliquée dans une OPAH. 


```{r tbl synthese com}
# on part de la table géo des OPAH
com_opah <- opah_4 %>% 
  # on en supprime la géométrie
  st_drop_geometry() %>% 
  # on ne garde que les OPAH en cours
  filter(temporalite == "encours") %>% 
  # on rassemble l'identifiant et le nom de l'opah dans une seule colonne
  mutate(programme = paste(id_opah, nom_court, sep = " - ")) %>% 
  # on ne garde que les colonne utiles à la table de synthèse"
  select(id_opah, type_programme, programme) %>% 
  # on ajoute la composition communale des OPAH
  left_join(opah_com_1) %>% 
  distinct() %>% 
  # on passe au format large pour avoir une colonne par type d'opah pour chaque commune
  pivot_wider(id_cols = depcom, names_from = type_programme, values_from = programme, names_sort = TRUE) %>% 
  # on ajoute toutes les communes de la région avec leur code EPCI
  full_join(communes %>% 
              filter(grepl("52", REGIONS_DE_L_EPCI)) %>% 
              select(DEPCOM, nom_commune = NOM_DEPCOM, EPCI, NOM_EPCI), 
            by = c("depcom" = "DEPCOM")) %>%
  arrange(depcom) %>% 
  relocate(nom_commune, .after = depcom) %>% 
  mutate(across(where(is.factor), as.character))

write.csv2(com_opah, file = paste0("../exports_pr_verif/OPAH/", today(), "_opah_com_synthese.csv"), 
           fileEncoding = "UTF-8", row.names = FALSE, na = "")
```


```{r tbl synthese com param sgbd}
# Versement sur le SGBD
schemaZh <- "zonages_habitat"
table <- "r_communes_opah_r52"
table_mil <- paste0(table, "_", params$annee)
commentaire <- paste0("Table communale de synthèse sur les OPAH, mise à jour le ", format(Sys.Date(), '%d %m %Y'), 
                    ". La carte communale est celle de ", params$annee, ".")
```

```{r tbl synthese com sgbd, eval=params$sgbd}
poster_data(com_opah, schema = schemaZh, table = table, pk = "depcom", db = "production", overwrite = TRUE)
commenter_table(comment = commentaire, table = table, schema = schemaZh, db = "production", user = "does")

poster_data(com_opah, schema = schemaZh, table = table_mil, pk = "depcom", db = "production", overwrite = TRUE)
commenter_table(comment = commentaire, table = table_mil, schema = schemaZh, db = "production", user = "does")

```

On crée une table communale non géographique que l'on charge sur le SGBD, dans la base PRODUCTION, schéma `r schemaZh`, sous les noms `r table` et  `r table_mil`.

## Création d'une table au standard covadis n_opah_zsup_r52 par agrégation des seuls contours communaux

Création d'une table par agrégation des contours communaux au standard covadis, défini [ici](http://geoinformations.metier.e2.rie.gouv.fr/fichier/pdf/COVADIS_standard_zonages_habitat_ville_planif_v1_cle598238.pdf?arg=140601537&cle=f0b4abfb83ba6f096f160b4265b77f188da1f522&file=pdf%2FCOVADIS_standard_zonages_habitat_ville_planif_v1_cle598238.pdf) et [là](http://geostandards.developpement-durable.gouv.fr/accederDonnees.do?direct=true&cle=Jeu%2FN_OPAH_ZSUP). 



```{r covadisation}
# calul des surfaces d'opah en km²
surf_opah <- opah_com_1 %>% 
  left_join(communes_geo, by = c("depcom" = "DEPCOM")) %>% 
  select(id_opah, superficie = AREA) %>% 
  group_by(id_opah) %>% 
  summarise(superficie = sum(units::drop_units(superficie)/1000000), .groups = "drop")

opah_covadis <- opah_3 %>% 
  left_join(surf_opah, by = "id_opah") %>% 
  mutate(duree = as.duration(interval(date_debut, date_expiration)) %>% as.numeric("years") %>% round(1), 
         date_convention = NA_Date_, 
         ref_convention = NA_character_, 
         id_map = NA_integer_) %>% 
  select(id_opah, nom, type = type_programme, date_convention, date_effet = date_debut, duree, ref_convention, superficie,
         nb_commune = nb_communes, id_map, the_geom = geometry)

```

```{r tbl covadis sgbd, eval=params$sgbd}
# Versement sur le SGBD
schemaZh <- "zonages_habitat"
table <- "n_opah_zsup_r52"
table_mil <- paste0("n_opah_zsup_", params$annee, "_r52")
commentaire <- paste0("Table des OPAH au standard covadis, obtenue par agrégation des contours communaux, mise à jour le ",
                      format(Sys.Date(), '%d %m %Y'), 
                      ". La carte communale est celle de ", params$annee, ".")

poster_data(opah_covadis, schema = schemaZh, table = table, pk = "id_opah", db = "production", overwrite = TRUE)
commenter_table(comment = commentaire, table = table, schema = schemaZh, db = "production", user = "does")

poster_data(opah_covadis, schema = schemaZh, table = table_mil, pk = "id_opah", db = "production", overwrite = TRUE)
commenter_table(comment = commentaire, table = table_mil, schema = schemaZh, db = "production", user = "does")

dico_attr_covadis <- tribble(
  ~var,	~lib_var,
  'id_opah',	"Identifiant unique national de l'OPAH fourni par l'ANAH",
  'nom',	"Dénomination de l'OPAH",
  'type',	"Type de l'OPAH",
  'date_convention',	"Date de signature de la convention passée entre l'État, l'ANAH et les collectivités concernées",
  'date_effet',	'Date à laquelle la convention prend effet',
  'duree',	"Durée de l'opération (en années)",
  'ref_convention',	"Référence (lien URL) vers le fichier numérique contenant la convention d'OPAH (non renseigné)",
  'superficie',	"Superficie du territoire couvert par l'OPAH en km2",
  'nb_commune',	'Nombre de communes concernées',
  'id_map',	'Identifiant technique à rajouter pour un stockage de la table en GéoBASE'
)
 
post_dico_attr(dico = dico_attr_covadis, table = table, schema = schemaZh, db = "production", user = "does")
post_dico_attr(dico = dico_attr_covadis, table = table_mil, schema = schemaZh, db = "production", user = "does")

save.image("OPAH.RData")

```
