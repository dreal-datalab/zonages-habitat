---
title: "Exploration des tables de zonages administrés par la DGALN via ASI"
author: "Juliette ENGELAERE-LEFEBVRE"
date: " 30 mars 2021 (actualisé : `r format(Sys.Date(), '%d/%m/%Y')`)" 
output: html_document
---

```{r setup, include=FALSE, message = FALSE}
# options de compilation rmd
knitr::opts_chunk$set(echo = TRUE)
options(knitr.kable.NA = '')

# chargements des packages
library(tidyverse)
library(lubridate)
# devtools::install_github("maeltheuliere/COGiter")
library(COGiter)
library(readxl)
library(RPostgres)
library(rpostgis)
library(DBI)
library(sf)
# devtools::install_github("pachevalier/tricky")
library(tricky)

# ouverture de la connexion au SGBD
drv <- Postgres()
con <- dbConnect(drv, dbname="production", host="10.44.128.174", port=5432, user = Sys.getenv("sgbd_user"),
                  password=Sys.getenv("sgbd_pwd"))

# chargement des tables 2021 perso en attendant COGiter 2021
load("C:/Users/juliette.engelaere/Documents/Travail/R_local/COGiter/COGiter/data-raw/2021/cog_com_2021.RData")
table_passage_com_historique <- tbl_com_com_hist %>% 
  # Ajout saint Barthelemy et St Martin
  bind_rows(data.frame(DEPCOM = c("97701", "97801", "97701", "97801"), DEPCOM_HIST = c("97123", "97127", "97701", "97801")))
communes <- communes_cog2021
communes_zon_etude <- COGugaison::table_supracom_2021
```

# Lecture des données du référentiel DGALN ASI

LA DGALN administre nationalement plusieurs zonages pour les verser à ses infocentres.
En attendant la récupération des zonages par API sur RefSIL, on lit les tableurs fournis par la DGALN issus du reférentiel ASI.
Cette exploration concerne les zonages :   

- ABC,   
- TLV,  
- Délégation de compétences,  
- Zonage HLM,   
- SRU.  

## Sélection des fichiers utiles

```{r lecture fichiers}

repo_ext_data <- "../../extdata/ASI/"
fich <- list.files(repo_ext_data, full.names = FALSE, recursive = TRUE)

fic <- list()

# fichier ABC, TLV, zonage local PDL
fic$zon_com_1 <- grep("commzonag", fich, value = TRUE) 
# fichier HLM 123, SRU
fic$zon_com_2 <- grep("commune", fich, value = TRUE)
# fichiers pour délégation de compétences (plus colonne code_gest de la table communale fic$zon_com_2)
fic$deleg1 <- grep("delegat", fich, value = TRUE)
fic$deleg2 <- grep("gestionn", fich, value = TRUE)
fic$deleg3 <- grep("typedeleg", fich, value = TRUE)
fic$deleg4 <- grep("categmoa", fich, value = TRUE)

adr <- paste0(repo_ext_data, fic)

```

La mise à jour des zonages administrés par la DGALN dans ASI va se baser sur les fichiers :    
- `r paste0(fic, collapse = ",<br>- ")`.

## Lecture de la table ABC_HLM_TLV_PDL  

La table `r fic$zon_com_1` comprend les zonages nationaux ABC, ABC rénové, ABC bis, HLM 123, TLV et le zonage local PDL 123.

```{r lecture des donnees, message=FALSE}
ABC_HLM_TLV_PDL_0 <- read_excel(adr[1], col_types = "text") 
ABC_HLM_TLV_PDL <- ABC_HLM_TLV_PDL_0 %>% 
  rename_with(~ gsub("Zona_", "", .x)) %>% 
  select(depcom_old = comm, contains("abc"), hlm_123 = `123`, tlv = tlv, pdl_123 = local) %>% 
  left_join(table_passage_com_historique, by = c("depcom_old" = "DEPCOM_HIST")) %>% 
  full_join(communes, by = c("DEPCOM")) %>% 
  mutate(pdl_123 = if_else(grepl("52", REGIONS_DE_L_EPCI), pdl_123, NA_character_ ),
         tlv = (tlv=="o")) %>% 
  select(-contains("DE_L_EPCI"), -contains("NCC"), -COMPARENT) %>% 
  add_count(DEPCOM, name = "nb_depcom")
  
```


### Vérification de la carte communale utilisée
```{r verif com ABC_HLM_TLV_PDL}
nb_doublons <- nrow(ABC_HLM_TLV_PDL) - n_distinct(ABC_HLM_TLV_PDL$DEPCOM)
com_mq <- filter(ABC_HLM_TLV_PDL, is.na(depcom_old))

```
La table chargée comprend `r nrow(ABC_HLM_TLV_PDL)` lignes (1 par commune), après mise à jour des codes communes, elle comprend `r n_distinct(ABC_HLM_TLV_PDL$DEPCOM)` codes communes distincts, la table fournie par la DGALN comprend donc `r nb_doublons` lignes qui correspondent à des communes ayant disparu ou inconnues.

Par ailleurs, il manque dans la table fournie par la DGALN `r nrow(com_mq)` communes valides en 2021 :  
- `r paste0(paste(com_mq$DEPCOM, com_mq$NOM_DEPCOM), collapse = ",<br>- ")`.


Pour autant est-ce que les lignes en doublons comportent des informations de zonages divergentes ?
```{r liste doublons ABC_HLM_TLV_PDL}
doublons_ABC_HLM_TLV_PDL <- group_by(ABC_HLM_TLV_PDL, DEPCOM, NOM_DEPCOM, REG) %>% 
  filter(nb_depcom > 1) %>% 
  summarise(across(c(abc:pdl_123), list(nb = n_distinct, liste = ~list(sort(unique(.x))))),
            com_old = list(depcom_old), .groups = "keep") %>% 
  mutate(synthese = sum(c_across(contains("nb")))) %>% 
  ungroup() %>% 
  filter(synthese > 6)
```
Oui, `r nrow(doublons_ABC_HLM_TLV_PDL)` communes présentent des zonages mixtes.
On exporte la table des doublons et des communes manquantes pour dialogue avec la DGALN.

### Exports des problèmes identifiés
```{r export doublons ABC_HLM_TLV_PDL}
doublons_ABC_HLM_TLV_PDL %>% 
  # on enlève la colonne sur les zonages locaux et on simplifie pour ne garder que les colonnes utiles au dialogue
  select(-contains("PDL"), -contains("_nb"), -synthese) %>% 
  rename_with(~gsub("_liste", "", .x)) %>% 
  # on reformate les colonnes de type liste en chaîne de caractères pour qu'elles puissent être exportées
  rowwise %>% 
  mutate(across(where(is.list), ~ paste(.x, collapse = ", "))) %>% 
  ungroup() %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Doublons_com_", gsub(".xlsx", "", fic$zon_com_1), ".csv"), quote_escape = "double")

com_mq %>% 
  select(DEPCOM:NOM_REG) %>% 
  mutate(fichier = fic$zon_com_1) %>% 
  relocate(fichier) %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Communes_manquantes_ASI_2021.csv"), append = FALSE)
```

Les communes manquantes sont exportées dans le fichier Communes_manquantes_ASI_2021.csv et les communes présentant des zonages mixtes sont exportées sous `r paste0("Doublons_com_", gsub(".xlsx", "", fic$zon_com_1), ".csv")`

### Comment faire autrement

Une table nationale diffusée par le ministère a été mise à jour en 2019 sur le zonage ABC : https://www.ecologie.gouv.fr/zonage-b-c.
https://www.ecologie.gouv.fr/sites/default/files/Zonage_abc_communes_2019.xls

sur TLV 
https://www.ecologie.gouv.fr/autres-zonages-des-politiques-du-logement
https://www.ecologie.gouv.fr/sites/default/files/La%20listes%20des%20communes%20%28taxe%20sur%20les%20logements%20vacants%29.xls

Une info difusée par le ministère a été mise à jour en sept 2020 sur le zonage 123
https://www.ecologie.gouv.fr/zonage-1-2-3
https://www.service-public.fr/simulateur/calcul/zones

## Lecture de la table SRU

La table `r fic$zon_com_2` comprend les zonages SRU : `art55_concer` et `art55_soum` (booléens). Quelle signification ?

```{r lecture SRU, message=FALSE}

SRU_0 <- read_excel(adr[2], col_types = "text")

SRU <- SRU_0 %>% 
  select(depcom_old = Comm_code, contains("art55"), Comm_annee_fin) %>% 
  rename_with(~ gsub("Comm_", "", .x)) %>% 
  left_join(table_passage_com_historique, by = c("depcom_old" = "DEPCOM_HIST")) %>% 
  full_join(communes, by = c("DEPCOM")) %>% 
  select(-contains("DE_L_EPCI"), -contains("NCC"), -COMPARENT) %>% 
  mutate(across(contains("art55"), ~.x == "o")) %>% 
  add_count(DEPCOM, name = "nb_depcom")
  
```


### Vérification de la carte communale utilisée
```{r verif com SRU}
nb_doublons_sru <- nrow(SRU) - n_distinct(SRU$DEPCOM)
com_mq_SRU <- filter(SRU, is.na(depcom_old))

```
La table chargée comprend `r nrow(SRU)` lignes (1 par commune), après mise à jour des codes communes, elle comprend `r n_distinct(SRU$DEPCOM)` codes communes distincts, la table fournie par la DGALN comprend donc `r nb_doublons_sru` lignes qui correspondent à des communes ayant disparu ou inconnues.

Cette fois il ne manque aucune communes actives en 2021 dans la table. Le champ `Comm_annee_fin` permet d'identifier les communes ayant disparu. En enlevant ces communes et les arrondissements de Paris Lyon Marseille. On obtient une table comprenant le même nombre de lignes que notre table de référence (34965 communes).


Pour autant est-ce que les lignes en doublons comportent des informations de zonages divergentes ? (on écarte préalablement les communes ayant disparu)
```{r liste doublons SRU}
doublons_SRU <- group_by(SRU, DEPCOM, NOM_DEPCOM, REG) %>% 
  filter(nb_depcom > 1, is.na(annee_fin)) %>% 
  summarise(across(contains("art55"), list(nb = n_distinct, liste = ~list(sort(unique(.x))))),
            com_old = list(depcom_old), .groups = "keep") %>% 
  mutate(synthese = sum(c_across(contains("nb")))) %>% 
  ungroup() %>% 
  filter(synthese > 2)


```
Si on écarte les communes ayant disparu, aucune commune actuelle ne présente de zonage mixte. (20 sinon) 
`r emo::ji("tada")`

```{r export doublons SRU}
# doublons_SRU %>% 
#   # on simplifie pour ne garder que les colonnes utiles au dialogue
#   select(-contains("_nb"), -synthese) %>% 
#   rename_with(~gsub("_liste", "", .x)) %>% 
#   # on reformate les colonnes de type liste en chaîne de caractères pour qu'elles puissent être exportées
#   rowwise %>% 
#   mutate(across(where(is.list), ~ paste(.x, collapse = ", "))) %>% 
#   ungroup() %>% 
#   write_csv2(file = paste0("../exports_pr_verif/ASI/", "Doublons_com_", gsub(".xlsx", "", fic$zon_com_2), ".csv"), quote_escape = "double")

com_mq_SRU %>% 
  select(DEPCOM:NOM_REG) %>% 
  mutate(fichier = fic$zon_com_2) %>% 
  relocate(fichier) %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Communes_manquantes_ASI_2021.csv"), append = TRUE)

```


## Lecture des tables délegataires des aides à la pierre

Les tables `r fic$deleg1`, `r fic$deleg2`, `r fic$deleg3`, `r fic$deleg4` et `r fic$zon_com_2` comprennent des informations relatives aux délégataires.

```{r lecture deleg, message=FALSE}

# chargement table communale des codes délégataires
deleg_com <- SRU_0 %>% 
  # on ne garde que les communes actives en 2021, en écartant les arrondissements de Paris, Lyon et Marseille
  filter(Comm_code != "99999", is.na(Comm_annee_fin), !grepl("arrondissement", Comm_nom)) %>%  
  select(depcom = Comm_code, code_gest) %>% 
  # correction du pain sur les DOM, avec des codes gestionnaires à DD097 au lieu de DD974 par exemple
  mutate(code_gest = if_else(code_gest=="DD097", paste0("DD", substr(depcom, 1, 3)), code_gest))


# chargements des autres tables liées aux délégataires
deleg_main_0 <- read_excel(adr[3], col_types = "text")
deleg_main <- deleg_main_0 %>% 
  select(-contains("adr"), -Dele_mel, -Dele_tel) %>% 
  mutate(Dele_siren = str_extract(Dele_siren, "\\d{9}"))
  
deleg_codes_0 <- read_excel(adr[4], col_types = "text")

deleg_type_0 <- read_excel(adr[5], col_types = "text")

deleg_cat_0 <- read_excel(adr[6], col_types = "text")
  
```

## Assemblage des informations éparses dans une seule table  
On les assemble comme on peut pour constituer une table communale des délégataires.

```{r jointure deleg, message=FALSE}

# on part de la table des gestionnaires (Galion ?)
deleg0 <- deleg_codes_0 %>% 
  # on ne garde que les gestionnaires délégataires + la DEAL de Mayotte
  filter(gest_deleg == "Oui" | gest_code %in% deleg_com$code_gest | gest_code == "DD976") %>% 
  select(-gest_deleg) %>% 
  # on joint cette table à la table issue de adr[3] (delegat2021.xlsx), cela a pour effet d'ajouter les colonnes Dele_type et type_delegat
  full_join(deleg_main, by = c("gest_code" = "Dele_codegest", "gest_depref" = "Dele_depref", "gest_siren" = "Dele_siren")) %>% 
  # on explicite le champ Dele_type en joignant la table typdeleg, cela a pour effet d'ajouter la colonnes tdel_nom, valant EPCI ou département - ok
  left_join(deleg_type_0, by = c("Dele_type" = "Tdel_code")) %>% 
  filter(gest_code != "DRNAT") %>% 
  full_join(deleg_com, by = c("gest_code" = "code_gest"))  
  
com_ss_deleg <- filter(deleg0, is.na(gest_code)) %>% 
  left_join(communes, by = c("depcom" = "DEPCOM")) %>% 
  select(depcom, NOM_DEPCOM)

```

Pour cela, on part de la table des gestionnaires (Galion ?) figurant dans `r fic[[4]]` , on ne garde que les gestionnaires délégataires (soit pcq le champ gest_deleg est à oui, soit que le code du gestionnaire figure dans la table communale, dans le cas des services de l'Etat).    

> Nota : dans la table communale `r fic[[2]]`, tous les codes gestionnaires DD des DOM sont à 'DD097' au lieu de 'DD971' par exemple, comme ils le sont par ailleurs dans la table `r fic[[4]]`, un redressement de ce champ a été nécessaire.  

On joint cette table à la table issue de `r fic[[3]]` comprenant les colonnes `Dele_type` (valant "1" pour EPCI ou "2" pour Departement, cf `r adr[5]`) et `type_delegat` ( valant "2" ou "3", signification ?).   
Les champs de jointure sont au nombre de 3 : "gest_code" = "Dele_codegest", "gest_depref" = "Dele_depref", "gest_siren" = "Dele_siren". 

> nota  : un nettoyage  du champ "Dele_siren" a été nécessaire, il comprenait des caractères non imprimables tels que '\\n' qui gênaient la jointure.).  

On attache enfin à cette table la table des communes en fonction du code délégataire.

## Des communes sans délégataires

### Constat

Les 17 communes de Mayotte et 7 communes de métropole n'ont pas de codes délégataires :  
- `r filter(com_ss_deleg, substr(depcom, 1, 3) != "976") %>% mutate(a = paste(depcom, NOM_DEPCOM)) %>% pull(a) %>% paste(collapse = ",<br>- ")`.

### Redressements
> On redresse en fonction du code EPCI pour la métropole et on affecte la DEAL de Mayotte aux communes de Mayotte, comme dans le reste des DOM ?

```{r redress deleg com orphelines}
# construction d'une table de passage entre code EPCI et un code de commune important de l'EPCI
table_epci_com_centre <- communes %>% 
  select(DEPCOM, NOM_DEPCOM, EPCI, NOM_EPCI) %>% 
  left_join(passer_au_cog_a_jour(pop2015, aggrege = TRUE, garder_info_supra = FALSE)) %>% 
  group_by(EPCI, NOM_EPCI) %>% 
  arrange(desc(pop2015)) %>% 
  summarise(DEPCOM_centre = first(DEPCOM), NOMCOM_centre = first(NOM_DEPCOM))

# Création d'une table de passage entre les codes des communes orphelines et leur codes délégataire
com_ss_deleg_redr <- com_ss_deleg %>% 
  left_join(communes %>% select(DEPCOM, EPCI), by = c("depcom" = "DEPCOM")) %>% 
  left_join(table_epci_com_centre, by = "EPCI") %>% 
  left_join(deleg0 %>% select(depcom, gest_code), by = c("DEPCOM_centre" = "depcom")) %>% 
  mutate(code_gest = if_else(substr(depcom, 1, 3) == "976", "DD976", gest_code))

# on l'ajoute à la table de passage communes / codes gestionnaires
deleg_com1 <- deleg_com %>% 
  filter(!is.na(code_gest)) %>% 
  bind_rows(com_ss_deleg_redr %>% select(depcom, code_gest))

# et on met à jour la table ensemblière des délégataires
deleg1 <- deleg_codes_0 %>% 
  # on ne garde que les gestionnaires délégataires + la DEAL de Mayotte
  filter(gest_deleg == "Oui" | gest_code %in% deleg_com1$code_gest) %>% 
  select(-gest_deleg) %>% 
  # on joint cette table à la table issue de adr[3] (delegat2021.xlsx), cela a pour effet d'ajouter les colonnes Dele_type et type_delegat
  full_join(deleg_main, by = c("gest_code" = "Dele_codegest", "gest_depref" = "Dele_depref", "gest_siren" = "Dele_siren")) %>% 
  # on explicite le champ Dele_type en joignant la table typdeleg, cela a pour effet d'ajouter la colonnes tdel_nom, valant EPCI ou département - ok
  left_join(deleg_type_0, by = c("Dele_type" = "Tdel_code")) %>% 
  filter(gest_code != "DRNAT") %>% 
  full_join(deleg_com1, by = c("gest_code" = "code_gest")) 

# cas des gestionnaires orphelins
delg_ss_com <- filter(deleg1, is.na(depcom))

```

## Des délégataires sans communes 

### Constat

`r nrow(delg_ss_com)` délégataires n'ont pas de communes attribuées :   
- `r mutate(delg_ss_com, a = paste(gest_code, gest_nom)) %>% pull(a) %>% paste(collapse = ",<br>- ")` 

Il s'agit a priori d'un pb de mise à jour de la table `r fic$zon_com_2`, la plupart des territoires concernés sont indiqués comme relevant de la DDT.M. et  les délégataires ont pris la compétence récemment entre  `r pull(delg_ss_com, gest_debut_deleg) %>% range() %>% paste(collapse = " et ")`.

### Redressements

> proposition de recoder l'aire de compétence des délégataires orphelins en fonction de la composition des EPCI pour les délégataires de type EPCI et de substituer la DDT au CG dans le cas d'un délégataire conseil départemental.

```{r redress deleg gest orphelins}
# le cas des EPCI d'abord : création d'un bout de table qui se substituera aux lignes communales dont le gestionnaire n'est pas à jour
delg_ss_com_epci <- delg_ss_com %>% 
  # des délégataires orphelins, on ne garde que les EPCI
  filter(Dele_type == "1") %>% 
  # on enlève la colonne code commune qui est vide
  select(-depcom) %>% 
  # on ajoute les codes des communes composant l'EPCI à partir de la table des communes à jour
  left_join(select(communes, EPCI, depcom = DEPCOM), by = c("gest_siren" = "EPCI"))

# Assemblage et redressement de la table des délégataires ensemblière, avant de s'attaquer aux CD (au cas ou un des délégataires EPCI appartiendrait à l'un des CD orphelin)
deleg2 <- deleg1 %>% 
  # on enlève les lignes à mettre à jour de la table initiale
  filter(!(depcom %in% delg_ss_com_epci$depcom), !is.na(depcom)) %>% 
  bind_rows(delg_ss_com_epci)

# le cas des Département : création d'un bout de table qui se substituera aux lignes communales dont le gestionnaire n'est pas à jour
delg_ss_com_cd <- delg_ss_com %>% 
  # des délégataires orphelins, on ne garde que les Départements
  filter(Dele_type == "2") %>% 
  # on enlève la colonne code commune qui est vide
  select(-depcom) %>% 
  # on ajoute un champ de jointure, comprenant le code gestionnaire de la DDT que remplace le CD
  mutate(gest_code_old = gsub("CG", "DD", gest_code)) %>% 
  left_join(select(deleg2, gest_code_old = gest_code, depcom)) %>% 
  select(-gest_code_old)
  
# Assemblage et redressement de la table des délégataires ensemblière
deleg3 <- deleg2 %>% 
  # on enlève les lignes à mettre à jour de la table initiale
  filter(!(depcom %in% delg_ss_com_cd$depcom)) %>% 
  bind_rows(delg_ss_com_cd)

```

## Des dates de fin de délégation dépassées
De nombreux délégataires dont la délégation est sensée être écoulée sont toujours dans la table communale : est-ce lié à un défaut de mise à jour des dates des délégations ou d'actualisation de la table communale ? 
```{r deleg perimees}
deleg_perimes <- filter(deleg3, gest_fin_deleg < "2021") %>% 
  select(gest_code, gest_nom, gest_fin_deleg) %>% 
  distinct() %>% 
  mutate(a = paste0(gest_code, " - ", gest_nom, " : ", gest_fin_deleg))
```
Cela concerne ``r nrow(deleg_perimes)` délégataires :  
- `r pull(deleg_perimes, a) %>% paste(collapse = ",<br>- ")`


## Vérification de la carte EPCI utilisée
Maintenant que la table des délégataire comprend autant de lignes que de communes actives en 2021, que toutes les communes ont un délégataire et que tous les délégataires ont une commune, on vérifie que la composition des EPCI est à jour.

###  Y a-t-il des codes EPCI de délégataires invalides en 2021 ?
```{r validite codes epci}

ls_epci_deleg <- filter(deleg3, Dele_type == "1") %>% 
  pull(gest_siren) %>% 
  unique()

!is_empty(setdiff(ls_epci_deleg, unique(communes$EPCI) %>% as.character))
```
Non, ouf.

### Communes affectées aux EPCI délégataires

Il convient de vérifier que les communes rattachées à un délégataire de type EPCI correspondent bien à la composition 2021 de cet EPCI (Pas de communes manquantes, pas de communes en trop)

```{r verif EPCI deleg}

# creation de la table de composition communale à jour des EPCI délégataires
com_epci_del <- filter(communes , EPCI %in% ls_epci_deleg) %>% 
  select(DEPCOM, NOM_DEPCOM, EPCI, NOM_EPCI)

deleg_pb_epci <- deleg3 %>% 
  # on ne garde que les communes sous délégation de leur EPCI
  filter(Dele_type == 1) %>% 
  select(depcom, gest_code, gest_nom, gest_siren) %>% 
  full_join(com_epci_del, by = c("gest_siren" = "EPCI", "depcom" = "DEPCOM")) %>% 
  filter(is.na(depcom) | is.na(NOM_DEPCOM))

# communes des EPCI délégataires qui n'aurait pas trouvé de correspondance dans la table de travail (éventuelles communes manquantes)
pb1 <- filter(deleg_pb_epci, is.na(depcom))

# communes de la table de travail qui ne sont pas dans le bon EPCI (éventuelles communes en trop)
pb2 <- filter(deleg_pb_epci, is.na(NOM_DEPCOM)) %>% 
  select(-NOM_DEPCOM, -NOM_EPCI) %>% 
  left_join(select(communes, DEPCOM = DEPCOM, NOM_DEPCOM, EPCI, NOM_EPCI), by = c("depcom" = "DEPCOM")) 
  

```
 `r nrow(pb1)` commune composant les EPCI délégataires (d'après le référentiel 2021) sont adossées à un autre délégataire dans la table de travail. Autrement dit : toutes les communes 2021 des EPCI délégataires sont bien affectées au bon EPCI.  
 
 `r nrow(pb2)` communes indiquées comme relevant d'un EPCI délégataire dans la table de travail relèvent en fait d'un autre EPCI (d'après le référentiel 2021). Autrement dit : des communes qui ne relèvent pas ou plus des EPCI délégataires le sont pourtant dans la table `r fic$zon_com_2` : 
```{r pb actu epci}
knitr::kable(pb2)
```

Redressement du code délégataire de ces `r nrow(pb2)` communes à l'image de ce qui à été fait précédemment à partir du bon code EPCI de chacune.
```{r corr pb EPCI}
# on part de la tables des communes mal rattachées
corr_epci <- pb2 %>% 
  # on adjoint à chacune le code de la commune principale de l'EPCI
  left_join(table_epci_com_centre, by = c("EPCI")) %>% 
  select(depcom, DEPCOM_centre) %>% 
  # avant d'adjoindre le gestionnaire de la commune centre à chaque commune à pb
  left_join(deleg3 %>% rename(DEPCOM_centre = depcom), by = c("DEPCOM_centre")) %>% 
  select(-DEPCOM_centre)
              
deleg4 <- deleg3 %>% 
  anti_join(corr_epci, by = "depcom") %>% 
  bind_rows(corr_epci)

```

Export de la table des délégataires consolidées sous 'Delegataires_com_2021.csv' et export de la liste des communes mal rattachées, en trop par rapport au périmètre actuel des EPCI, sous 'Communes_mauvais_delegataire.csv'.

```{r export table_deleg}
deleg4 %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Delegataires_com_2021.csv"), quote_escape = "double")

pb2 %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Communes_mauvais_delegataire.csv"), append = TRUE)

```

### Comment créer plus simplement la table des zones des délégataires chaque année ?

Si la date de fin de délégation est une donne fiable, le process de mise à jour annuel suivant est peut être plus pertinent.

Partir de la la table `r fic[[4]]`, listant les délégataires et comprenant leurs noms, codes, SIREN, types (EPCI, Département), écarter les délégataires dont la date de fin de délégation est dépassée, joindre aux délégataires EPCI la composition communale de chacun, joindre aux départements délégataires les communes non encore jointes, affecter les communes restantes aux services départementaux de l'Etat.

```{r aller on le fait}

delegataires <- deleg_codes_0 %>% 
  filter(gest_deleg == "Oui") %>% 
  select(-gest_deleg) %>% 
  # ajout du type de delegataire
  left_join(deleg_main %>% select(Dele_codegest, Dele_siren, Dele_type), 
            by = c("gest_code" = "Dele_codegest", "gest_siren" = "Dele_siren")) %>% 
  left_join(deleg_type_0, by = c("Dele_type" = "Tdel_code"))

# association des communes aux EPCI délégataires
comm_deleg_epci <- filter(delegataires, tdel_nom == "EPCI") %>% 
  left_join(select(communes, DEPCOM, NOM_DEPCOM, EPCI), by = c("gest_siren" = "EPCI")) %>% 
  select(DEPCOM, NOM_DEPCOM, gest_code, gest_nom)

# association des communes aux CD délégataires
comm_deleg_dep <- filter(delegataires, tdel_nom == "departement") %>% 
  # on apparie d'abord avec toutes les communes du département
  left_join(select(communes, DEPCOM, NOM_DEPCOM, DEP), by = c("gest_depref" = "DEP")) %>% 
  # puis on enlève les lignes communales déjà sous gestionnaire EPCI
  anti_join(comm_deleg_epci, by = "DEPCOM") %>% 
  select(DEPCOM, NOM_DEPCOM, gest_code, gest_nom)

# association des communes à leur services départementaux

comm_svc_dep <- select(communes, DEPCOM, NOM_DEPCOM, DEP) %>% 
  bind_rows(data.frame(DEPCOM = c("97701", "97801"), NOM_DEPCOM = c("Saint-Barthélemy", "Saint-Martin"), DEP = c("977", "978"))) %>% 
  mutate(gest_code = if_else(nchar(as.character(DEP)) == 2, paste0("DD0", DEP),  paste0("DD", DEP))) %>% 
  left_join(select(deleg_codes_0, gest_code, gest_nom), by = "gest_code") %>% 
  select(-DEP) %>% 
  # on enlève ce qui est déjà présent dans les 2 premières tables
  anti_join(comm_deleg_epci, by = "DEPCOM") %>% 
  anti_join(comm_deleg_dep, by = "DEPCOM")

# Compilation des trois tables communales

comm_deleg <- bind_rows(comm_deleg_epci, comm_deleg_dep, comm_svc_dep) %>% 
  arrange(DEPCOM)

# Exports csv
comm_deleg %>% 
  write_csv2(file = paste0("../exports_pr_verif/ASI/", "Delegataires_com_2021_", Sys.Date(),".csv"), quote_escape = "double")


```


