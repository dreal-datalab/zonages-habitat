# Zonages habitat

La plupart des politiques publiques de l’habitat s'appuient sur des zonages qui changent au cours du temps, en fonction des politiques mises en oeuvre et des évolutions des périmètres institutionnels.    

On distingue deux types de zonages :   
- ceux définis au niveau national par décret ou arrêté,   
- et les zonages "thématiques" liés aux dispositifs : SRU, PLH, OPAH, PIG…  


Les deux principaux enjeux du projets sont de :   
- partager la connaissance habitat "actualisée" au travers de la diffusion la plus large possible des zonages,   
-  faciliter la création, la mise à jour et l'utilisation des zonages pour porter les politiques publiques de l'habitat.    